﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{     
    [SerializeField]
    private Stat health;

    [SerializeField]
    private Stat mana;

    public GameObject Enemy;

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.tag == "Player")
        {
            health.CurrentVal -= 10;
        }
    }


    private void Awake()
    {
        health.Initialize();
        mana.Initialize();
    }

      
// Update is called once per frame
void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Q))
        {
            health.CurrentVal -= 10;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            health.CurrentVal += 10;
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            mana.CurrentVal -= 10;
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            mana.CurrentVal += 10;
        }
       
    }
}